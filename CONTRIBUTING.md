Thanks for your interest in helping to improve this project!

The best way to get in touch to discuss what you could help with is to [open
a new issue](https://gitlab.com/joelostblom/context-explorer/issues/new) or
comment on one in [the issue
list](https://gitlab.com/joelostblom/context-explorer/issues). You can also Some
ideas on how you can help:

- General feedback and suggestions
- Testing to use the software
- Implementing new features
- Improving existing features
- Code improvements
- Help setting up tests and continuous integrations

If you don't already know what you want to help with, you can look at the issue
list and keep an eye out for items labelled as
[good-first-issue](https://gitlab.com/joelostblom/context-explorer/issues?label_name%5B%5D=good-first-issue).
