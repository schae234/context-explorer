# ContextExplorer

Thanks for stopping by! `（ ^_^）o自自o（^_^ ）`

ContextExplorer is a software program that facilitates analyses of data
extracted from microscope images of cells.

## Relevance

The analyses methods in ContextExplorer focuses on how populations of cells are
affected by their microenvironment, including local variations in cell
signalling and cell density. It is currently difficult for scientists without
previous programming experience to quantify these variables, although it is
relevant for many research areas. Facilitating this type of analyses can help
scientists around the world to improve their understanding of cellular behavior
and accelerate their research.

## Overview

ContextExplorer is controlled via a graphical user interface and aims to enable
powerful analysis and visualizations of single cell data extracted from
microscope images for a broad scientific audience. ContextExplorer can
work in tandem with many other tools since it only depends on a correctly
formatted CSV-file as input and only outputs commonly used file formats (`.csv`,
`.jpg`, `.png`, and `.pdf`)

## Getting started with ContextExplorer

### Installation

ContextExplorer can be installed via the package managers `conda` or `pip`. The
recommended way is to use `conda`.

#### Conda installation

1. Download and install the [Anaconda Python
   distribution](https://www.anaconda.com/download/) (version 3.x). This is an
   easy way to install Python and gives access to the powerful package manager
   `conda`.
2. If you are using Windows, open up the `Anaconda Command Prompt` from the start menu.
   On MacOS and Linux you can use your default terminal (e.g. `terminal.app` on
   MacOS).
3. Type `conda install -c joelostblom context_explorer` and press return.

That's it! You can run the program by typing `context_explorer` into the
terminal/Anaconda Command Prompt.

### Usage

To try out ContextExplorer, first download [the sample
data](https://gitlab.com/stemcellbioengineering/context-explorer/raw/master/sample-data/ce-sample.csv)
(right click link -> Save as). When you launch the program, choose this file
(or your own data) from the file selector. That's all you need to start testing
ContextExplorer!

Detailed documentation and workflow examples are available at the [documentation
page](http://contextexplorer.readthedocs.io/en/latest/).

<!--
Currently, the software is distributed as source code only. The required
dependencies are `python3`, `pandas`, `numpy`, `scikit-learn`, `matplotlib`,
`shapely`, and `natsort`. A `conda` package will be created shortly to
facilitate installation.
-->

## Contributing

Feedback and suggestions are always welcome! This does not have to be
code-related, don't be shy =) Please read [the contributing
guidelines](https://gitlab.com/joelostblom/context-explorer/blob/master/CONTRIBUTING.md)
to get started.

Detailed documentation and workflow examples are available at the [documentation
page](http://contextexplorer.readthedocs.io/en/latest/).
